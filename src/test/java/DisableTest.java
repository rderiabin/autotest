import TestNGOnePack.Action;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;


public class DisableTest extends Action {
    By HeaderCheckingText = By.xpath("/html/body/div[1]/section[2]/div[3]/aside/h3");
    By ProjectsSupportLink = By.xpath("/html/body/div[1]/section[1]/section/div[3]/a");



    @BeforeSuite
        public void setUp() throws InterruptedException {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test (groups = { "functest", "checkintest" })
        public void autotesting() throws InterruptedException {
            driver.get(AbstractPage.BaseURL);
            this.Click(ProjectsSupportLink);
            Thread.sleep(2000);
            this.Click(ProjectsSupportLink);
            Thread.sleep(2000);
            if (!driver.findElement(HeaderCheckingText).isDisplayed()){
                System.out.println("ЭЛЕМЕНТ НЕ НАЙДЕН");
            } else {
                System.out.println("ЭЛЕМЕНТ НАЙДЕН");
            }

    }

    @AfterSuite
    public void tearDown() throws Exception {
        driver.quit();
    }

}
