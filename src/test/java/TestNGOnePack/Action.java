package TestNGOnePack;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Action {

    protected static WebDriver driver;



    public void Click(By element) throws InterruptedException {
        new WebDriverWait(driver, 20)
                .until(ExpectedConditions.visibilityOfElementLocated(element)).click();
        Thread.sleep(4000);
    }

}

